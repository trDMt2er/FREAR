# -----------------------------------------------------------------------------
#   source('FREAR/R_scripts/maxPSR.R')
# -----------------------------------------------------------------------------

# Calculate the maximum-in-time PSR for each grid box of the output domain
# for a given srs field and given observations
# srs: array with SRS values [ntimes, nx, ny, nsamples]
# obs: vector of observations
# method: pearson, spearman (see ?cor)
calc_maxPSR <- function(srs, obs, method = "spearman") {
  h <- function(x, obs, method) {
    if(sum(abs(x)) == 0) 0.
    else cor(x, obs, method = method)
  }
  aux <- apply(srs, c(1,2,3), h, obs, method)
  maxPSR <- apply(aux, c(2,3), max)
  maxPSR
}

write_maxPSR <- function(maxPSR, maxPSR_pearson = NULL, settings, lsave) {
  # Write R objects to file for diagnostics later
  if(lsave) {
    saveRObject(maxPSR, directory = settings$outpath)
    if(!is.null(maxPSR_pearson)) saveRObject(maxPSR_pearson, directory = settings$outpath)
  }

  # Plot max PSR
  if(settings$lusemeteogrid) {
    plotfname <- paste(settings$outpath, "PSR_OLD.pdf", sep = "/")
    openPlot(lsave = lsave, plotfname = plotfname, width = 8,
             height = 8*(settings$domain$ny/settings$domain$nx)^(2/3))
    par(mar = c(2, 2, 4, 1))
    title <- 'maximum-in-time PSR'
    plot_maxPSR(maxPSR, domain = settings$domain, title = title, 
                IMSfile = settings$IMSfile, reactorfile = settings$reactorfile)
    if(!is.null(settings$fplotalso)) settings$fplotalso()
    closePlot(lsave = lsave, plotfname = plotfname)
  }

  # Plot max PSR pearson
  if(!is.null(maxPSR_pearson) && settings$lusemeteogrid) {
    plotfname <- paste(settings$outpath, "PSR_Pearson_OLD.pdf", sep = "/")
    openPlot(lsave = lsave, plotfname = plotfname, width = 8,
             height = 8*(settings$domain$ny/settings$domain$nx)^(2/3))
    par(mar = c(2, 2, 4, 1))
    title <- 'maximum-in-time PSR'
    plot_maxPSR(maxPSR_pearson, domain = settings$domain, title = title,
                IMSfile = settings$IMSfile, reactorfile = settings$reactorfile)
    if(!is.null(settings$fplotalso)) settings$fplotalso()
    closePlot(lsave = lsave, plotfname = plotfname)
  }

}

plot_maxPSR <- function(maxPSR, domain, title = "", IMSfile = NULL, reactorfile = NULL) {
  legendColors <- rev(c("red","yellow","royalblue1","white"))
  legendColors <- colorRampPalette(legendColors, space = "Lab")
  levels <- seq(0, 1, by = 0.1)
  plot_2Dplume(plume = maxPSR, domain = domain, IMSfile = IMSfile, stat_number = NULL,
               reactorfile = reactorfile, levels = levels, title = title, color.palette = legendColors,
               legend.width = 1.5/9, legend.cex = 1.3)
}
