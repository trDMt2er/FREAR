# -----------------------------------------------------------------------------
###############################################################################
#	(C) 2020 Her Majesty the Queen in Right of Canada, as represented by the
#	Minister of Health
#	
#	This program is free software; you can redistribute it and/or modify it 
#	under the terms of the GNU General Public License as published by the Free
#	Software Foundation; either version 3 of the License, or (at your option)
#	any later version.
#	
#	This program is distributed in the hope that it will be useful, but WITHOUT
#	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
#	FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
#	more details.
#	
#	You should have received a copy of the GNU General Public License along 
#	with this program; if not, see <https://www.gnu.org/licenses>.
###############################################################################
# source('FREAR/R_scripts/bayes.R')
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------

# General prior function factory for any multidimensional uniform prior
density_ffact <- function(upper_bayes, lower_bayes) {
  # force(upper_bayes) ; force(lower_bayes)
  function(par) {
    npar <- length(par)
    if(npar != length(lower_bayes) || npar != length(upper_bayes) ) {
      stop("Length of 'par' differs from length of 'upper_bayes' and 'lower_bayes' \n")
    }
    zdens <- 0
    # Note: it seems better to keep rstop uniform rather than tstop:
    # If rstart is fixed, then tstop will be uniform, which seems good behavior
    # If rstart is uniform, then tstop will be 'triangle distribution'
    # THUS DON'T USE: if(npar >= 5) lower_bayes[5] <- -par[4]/(1-par[4])
    for(ipar in 1:npar) {
      if(lower_bayes[ipar] == upper_bayes[ipar]) {
        aux <- 0
      } else if(lower_bayes[ipar] > upper_bayes[ipar]) {
        # NOT TESTED
        # This should only happen for the parameter longitude in case the domain encompasses the date line
        if(ipar < lower_bayes[ipar]) {
          aux <- dunif(par[ipar], min=-179.9999999, max=lower_bayes[ipar], log=TRUE)
        } else {
          aux <- dunif(par[ipar], min=upper_bayes[ipar], max=180, log=TRUE)
        }
      } else {
        aux <- dunif(par[ipar], min=lower_bayes[ipar], max=upper_bayes[ipar], log=TRUE)
      }
      zdens <- zdens + aux
    }
    zdens
  }
}

# General prior sampler function factory for any multidimensional uniform prior
sampler_ffact <- function(upper_bayes, lower_bayes) {
  # force(upper_bayes) ; force(lower_bayes)
  function(n=1) {
    npar <- length(upper_bayes)
    zsample <- array(0,dim=c(n,npar))
    for(ipar in 1:npar) {
      if(lower_bayes[ipar] > upper_bayes[ipar]) {
        # NOT TESTED
        # This should only happen for the parameter longitude in case the domain encompasses the date line
        # First, test on which side of the date line, then draw sample from that part of the domain
        drawEast <- function() runif(1, min=lower_bayes[ipar], max=180)
        drawWest <- function() runif(1, min=-179.99999, max=upper_bayes[ipar])
        lEast <- runif(n) < (180 - lower_bayes[ipar])/(360 + upper_bayes[ipar] - lower_bayes[ipar])
        #zsample[,ipar] <- ifelse( lEast, drawEast(), drawWest() ) # recycles values of drawEast(), drawWest() DO NOT USE
        zsample[,ipar] <- vapply(lEast, function(x) if(x) drawEast() else drawWest(), FUN.VALUE=1 )
      } else {
        zsample[,ipar] <- runif(n, min=lower_bayes[ipar], max=upper_bayes[ipar])
      }
    }
    zsample
  }
}



# -----------------------------------------------------------------------------

# EXPERIMENTAL
# Prior density is VG
density_ffact4 <- function(upper_bayes,lower_bayes, ACfun_bayes) {
  force(upper_bayes) ; force(lower_bayes) ; force(ACfun_bayes)
  function(par) {
    if(any(par < lower_bayes) || any(par > upper_bayes)) -Inf
    ac <- ACfun_bayes(par=par)
    sigma_tot <- sqrt(ac$sigma_obs^2+ac$sigma_mod^2)
    -sum( (log(ac$obs + sigma_tot) - log(ac$mod + sigma_tot))^2 )
  }
}

# -----------------------------------------------------------------------------

# Based on createPrior from BayesianTools
# sampler is already parallel
# density is made parallel here
makePrior <- function(density, sampler, lower_bayes, upper_bayes, best = NULL) {
  force(density) ; force(sampler) ; force(lower_bayes) ; force(upper_bayes) ; force(best)
  if( any(lower_bayes > upper_bayes) ) stop("Prior has lower_bayes > upper_bayes!")
  if(is.null(best)) best = (upper_bayes + lower_bayes)/2

  priorWrapper <- function(x) {
    if (!is.null(lower_bayes)) {
      if (any(x < lower_bayes))  return(-Inf)
    }
    if (!is.null(upper_bayes)) {
      if (any(x > upper_bayes))  return(-Inf)
    }
    out <- tryCatch({
                      density(x)
                    }, error = function(cond) {
                      warning("Problem in the prior", cond)
                      return(-Inf)
                   })
    if (out == Inf) stop("Inf encountered in prior")
    return(out)
  }
  parallelDensity <- function(x) {
    if(is.vector(x)) return( priorWrapper(x) )
    else if(is.matrix(x)) return( apply(x, 1, priorWrapper) )
    else stop("Parameter must be a vector or a matrix")
  }
  out <- list(density = parallelDensity, sampler = sampler, lower_bayes = lower_bayes,
              upper_bayes = upper_bayes, best = best)
  return(out)
}
# ------------------------------------------------------------------------------


# Based on createLikelihood from BayesianTools
# Capture possible errors in likelihood calculation
makeLikelihood <- function(likelihood) {
  force(likelihood)
  likelihoodWrapper <- function(x) {
    out <- tryCatch({
                      y = likelihood(x)
                      if (any(y == Inf | is.nan(y) | is.na(y) | !is.numeric(y))) {
                        message(paste("BayesianTools warning: positive Inf or NA / nan values,
                                      or non-numeric values occured in the likelihood.
                                      Setting likelihood to -Inf.\n Original value was",
                                      y, "for parameters", x, "\n\n "))
                        y[is.infinite(y) | is.nan(y) | is.na(y) | !is.numeric(y)] = -Inf
                      }
                      y
                    }, error = function(cond) {
                      message("*** Problem encountered in the calculation of the likelihood ***",
                      "\n* Error message was: ", cond,
                      "\n* Parameter values were: ", paste(x, collapse = " "),
                      "\n* Set result of the parameter evaluation to -Inf ",
                      "\n***************************************************************")
                      return(-Inf)
                   })
    return(out)
  }
  parallelDensity <- function(x) {
    if(is.vector(x)) return( likelihoodWrapper(x) )
    else if (is.matrix(x)) return( apply(x, 1, likelihoodWrapper) )
    else stop("Parameter must be a vector or a matrix")
  }
  out <- list(density = parallelDensity)#, sampler = sampler, cl = cl, pwLikelihood = pwLikelihood, parNames = names)
  return(out)
}
# ------------------------------------------------------------------------------


# Based on createPosterior from BayesianTools
# beta: coolness parameter for simulated annealing
#      Post <- Prior * Lik^beta
#      so: log(Post) <- log(Prior) + log(Lik)*beta
makePosterior <- function(prior, likelihood, beta = 1) {
  force(prior) ; force(likelihood) ; force(beta)
  posterior <- function(x) {
    if(is.vector(x)) {
      pr <- po <- prior$density(x)
      if (pr != -Inf) {
        ll <- likelihood$density(x)*beta
        po <- po + ll
      } else ll <- NA #-Inf
      return(c(pr,ll,po))
    } else if (is.matrix(x)) {
      pr <- prior$density(x)
      iaccept <- (pr != -Inf)
      if(dim(x)[2] == 1) {
        ll <- likelihood$density(matrix(x[iaccept,], ncol=1))*beta # why needed, why not just x?
      } else {
        if(TRUE %in% iaccept) {
          ll <- likelihood$density(x[iaccept,])*beta
        } else {
          ll <- -Inf
        }
      }
      post_out <- ll_out <- pr
      ll_out[!iaccept] <- NA
      ll_out[iaccept] <- ll
      post_out[!iaccept] <- -Inf
      post_out[iaccept] <- post_out[iaccept] + ll
      out <- cbind(pr, ll_out, post_out)
      colnames(out) <- c("prior","likelihood","posterior")
      return(out)

    } else {
      stop("Parameter must be a vector or a matrix")
    }
  }
  out <- list(density = posterior)
  return(out)
}
# ------------------------------------------------------------------------------

