# -----------------------------------------------------------------------------
###############################################################################
#	(C) 2020 Her Majesty the Queen in Right of Canada, as represented by the
#	Minister of Health
#	
#	This program is free software; you can redistribute it and/or modify it 
#	under the terms of the GNU General Public License as published by the Free
#	Software Foundation; either version 3 of the License, or (at your option)
#	any later version.
#	
#	This program is distributed in the hope that it will be useful, but WITHOUT
#	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
#	FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
#	more details.
#	
#	You should have received a copy of the GNU General Public License along 
#	with this program; if not, see <https://www.gnu.org/licenses>.
###############################################################################
# source('FREAR/R_scripts/ll_Yee2017_usinglog.R')
# -----------------------------------------------------------------------------

# Precalculate certain values of the likelihood for speed
preCalc_ll_noRelease <- function(lower_bayes, upper_bayes, ACfun_bayes, ll_aux = list("alphas" = 1/pi, "betas" = 1,
                                                                    "ll_noRelease" = NULL,
                                                                    "Ys" = NULL)) {
  param_noRelease <- apply(cbind(upper_bayes,lower_bayes),c(1),mean)
#  param_noRelease[3] <- -Inf # since Q <- 10^Q
  param_noRelease[which(grepl("log10_Q",settings$parnames))] <- -Inf # since Q <- 10^Q
  ac_noRelease <- ACfun_bayes(param_noRelease)
  ll_noRelease <- getLikes_Yee2017log(ac_noRelease, ll_aux)$likes
  ll_noRelease
}

preCalc_ll_factors <- function(lower_bayes, upper_bayes, ACfun_bayes, ll_aux = list("alphas" = 1/pi, "betas" = 1,
                                                                  "ll_noRelease" = NULL,
                                                                  "Ys" = NULL)) {
  ac_dummy <- ACfun_bayes(apply(cbind(upper_bayes,lower_bayes),c(1),mean))
  ll_factors <- rep(1, len = length(ac_dummy$obs))
  for(iobs in seq_along(ac_dummy$obs)) {
    if(ac_dummy$obs[iobs] == 0) {
      sigma_obs <- ac_dummy$sigma_obs[iobs]
      MDC <- ac_dummy$MDC[iobs]
      sigma_mod <- ac_dummy$sigma_mod[iobs]
      ac1 <- list("obs"=0  , "sigma_obs"=sigma_obs, "MDC"=MDC, "mod"=MDC/2, "sigma_mod"=sigma_mod)
      ac2 <- list("obs"=0  , "sigma_obs"=sigma_obs, "MDC"=MDC, "mod"=0    , "sigma_mod"=sigma_mod)
      ac3 <- list("obs"=MDC, "sigma_obs"=sigma_obs, "MDC"=MDC, "mod"=MDC/2, "sigma_mod"=sigma_mod)
      ac4 <- list("obs"=MDC, "sigma_obs"=sigma_obs, "MDC"=MDC, "mod"=MDC  , "sigma_mod"=sigma_mod)
      deltall1 <- getLikes_Yee2017log(ac = ac1, ll_aux = ll_aux)$likes - getLikes_Yee2017log(ac = ac2, ll_aux = ll_aux)$likes
      deltall2 <- getLikes_Yee2017log(ac = ac3, ll_aux = ll_aux)$likes - getLikes_Yee2017log(ac = ac4, ll_aux = ll_aux)$likes
      ll_factors[iobs] <- deltall2 / deltall1
    }
  }
  ll_factors
}


getLikes_Yee2017log <- function(ac, ll_aux = list("alphas" = 1/pi, "betas" = 1, 
                                                  "ll_noRelease" = NULL, 
                                                  "Ys" = NULL, "ll_factors" = NULL)) {
  # Check input in ll_aux
  if(is.numeric(ll_aux$alphas) && length(ll_aux$alphas)==1) {
    alphas <- rep(ll_aux$alphas,len=length(ac$obs))
  } else {
    alphas <- ll_aux$alphas
  }

  if(is.numeric(ll_aux$betas) && length(ll_aux$betas)==1) {
    betas <- rep(ll_aux$betas,len=length(ac$obs))
  } else {
    betas <- ll_aux$betas
  }

  ll_noRelease <- ll_aux$ll_noRelease
  
  if(is.null(ll_aux$Ys)) {
    Ys <- alphas^betas * gamma(betas+0.5) / (sqrt(2*pi) * gamma(betas))
  } else {
    Ys <- ll_aux$Ys
  }

  if(is.null(ll_aux$ll_factors)) {
    ll_factors <- rep(1., len=length(ac$obs))
  } else {
    ll_factors <- ll_aux$ll_factors
  }

  # Calculate likelihood for detections and non-detections separately
  idet <- which(ac$obs!=0)
  inondet <- which(ac$obs==0)
  ac_det <- getACsubset(ac,idet)
  ac_nondet <- getACsubset(ac,inondet)
  likes <- numeric(len=length(ac$obs))
  if(length(idet) != 0) {
    likes[idet] <- ll_Yee2017log_det(ac = ac_det, alphas[idet], betas[idet],
                                     Ys[idet], ll_noRelease[idet])
  }
  if(length(inondet) != 0) {
    likes[inondet] <- ll_Yee2017log_nondet(ac = ac_nondet, alphas[inondet],
                                           betas[inondet], Ys[inondet], 
                                           ll_noRelease[inondet])
  }

  likes <- likes * ll_factors

  list("likes"=likes)
}


# Auxiliary functions ----------------------------------------------------------

dctrue <- function(ctrue, logmod, sigma_tot, eps, Y, alpha, beta) {
  logctrue <- log(ctrue + eps)
  logs <- log(ctrue + eps + sigma_tot) - logctrue
  Y / (logs * (alpha + (logctrue - logmod)^2 / (2 * (logs)^2) )^(beta+0.5))
}

# Probability of a non-detect given a true concentration ctrue
# pnondet(c(0,1,2),1) = 0.95001509 0.50000000 0.04998491
# k_alpha = 1.645 for an alpha of 5%
pndet_ctrue <- function(ctrue, L_C, k_alpha = 1.645) {
  pnorm(L_C, mean = ctrue, sd = L_C/k_alpha)
}

# Probability of a detection given a true concentration ctrue
# pdet(c(0,1,2),1) = 0.04998491 0.50000000 0.95001509
# k_alpha = 1.645 for an alpha of 5%
pdet_ctrue <- function(ctrue, L_C, k_alpha = 1.645) {
  1 - pndet_ctrue(ctrue = ctrue, L_C = L_C, k_alpha = k_alpha)
}

# Probability of a true detection given c_mod
ptruedet_cmod <- function(L_C, logmod, sigma_tot, eps, Y, alpha, beta, 
                           zdctruenorm, upper = 20 * L_C) {
  integrate(dctrue, lower = L_C, upper = upper, logmod = logmod, 
            sigma_tot = sigma_tot, eps = eps, Y = Y, alpha = alpha,
            beta = beta)$value / zdctruenorm 
}

# Probability of a true non-detection given c_mod
ptruendet_cmod <- function(L_C, logmod, sigma_tot, eps, Y, alpha, beta, 
                           zdctruenorm) {
  integrate(dctrue, lower = 0, upper = L_C, logmod = logmod, 
            sigma_tot = sigma_tot, eps = eps, Y = Y, alpha = alpha,
            beta = beta)$value / zdctruenorm 
}

# Probability of a detecting c_det given a true non-detection (false alarm)
pcdet_truendet <- function(cdet, L_C, k_alpha = 1.645) {
  aux <- pnorm(0, mean = cdet, sd = L_C/k_alpha)
  (pnorm(L_C, mean = cdet, sd = L_C/k_alpha) - aux) / (1 - aux)
}

# Likelihood of a non-detection (true non-detection or miss) given c_mod
pndet_cmod <- function(L_C, logmod, sigma_mod, eps, Y, alpha, beta, zdctruenorm) {
  MDC <- 2 * L_C
  f2int <- function(ctrue, L_C, logmod, sigma_tot, eps, Y, alpha, beta, 
                    zdctruenorm) {
    pndet_ctrue(ctrue = ctrue, L_C = L_C) * dctrue(ctrue = ctrue, logmod = logmod,
                                                   sigma_tot = sigma_tot, 
                                                   eps = eps, Y = Y, alpha = alpha,
                                                   beta = beta) / zdctruenorm
  }
  integrate(f2int, lower = 0, upper = 10*MDC, L_C = L_C, logmod = logmod, 
            sigma_tot = sigma_mod, eps = eps, Y = Y, alpha = alpha, beta = beta, 
            zdctruenorm = zdctruenorm)$value
}




# ------------------------------------------------------------------------------


# Likelihood for detections -----------------------------------------------------
ll_Yee2017log_det <- function (ac, alphas = 1/pi, betas = 1, Ys = NULL, 
                               ll_noRelease = NULL) {
  # Init
  nsamples <- length(ac$obs)
  if(is.null(Ys)) {
    if(length(alphas) == 1) alphas <- rep(alphas, len = nsamples)
    if(length(betas) == 1) betas <- rep(betas, len = nsamples)
    Ys <- alphas^betas * gamma(betas+0.5) / (sqrt(2*pi) * gamma(betas))
  }
  eps <- 10^-6
  sigma_tot <- sqrt(ac$sigma_obs^2+ac$sigma_mod^2)
  MDC <- ac$MDC
  L_C <- ac$MDC/2
  likes <- numeric(len = nsamples)

  # If precalculated values of ll exist (for ac$mod == 0), then make use of it
  # Else, treat as if all ac$mod != 0, thus calc ll here
  if( !is.null(ll_noRelease)) {
    iprecalc <- which(ac$mod==0)
    icalc <- which(ac$mod!=0)
    likes[iprecalc] <- ll_noRelease[iprecalc]
  } else {
    icalc <- 1:nsamples
  }

    # Calculate ll for all icalc
  if( length(icalc) > 0 ) {
    # If probability of false alarm is insignificant, the calculation can be much
    # faster
    icalcfast <- which(ac$obs > L_C*5 & ac$mod > L_C*5)
    icalc <- setdiff(icalc, icalcfast)
    logmod <- log(ac$mod+eps)
    if( length(icalcfast) > 0 ) {
      # Assuming a detection, the likelihood of c_det given c_mod
      llcdet_cmod <-  dctrue(ctrue = ac$obs[icalcfast], logmod = logmod[icalcfast],
                             sigma_tot = sigma_tot[icalcfast], eps = eps, 
                             Y = Ys[icalcfast], alpha = alphas[icalcfast], 
                             beta = betas[icalcfast])
      likes[icalcfast] <- log(llcdet_cmod)
    }

    # Slow calculation including possibility of false alarm
    if( length(icalc) > 0 ) {
      # below: optimal limits for the integration
      lowLim <- ifelse(ac$mod < MDC, 0, pmax(0,ac$mod-100*ac$sigma_mod) )
      uppLim <- ifelse(ac$mod < MDC, MDC*10, 
                       pmax(MDC*10,ac$mod + pmin(10*ac$mod,100*ac$sigma_mod)))
      zdctruenorm <- rep(1., len = nsamples)
#      zdctruenorm[icalc] <- Vectorize( function(i) {
#                                        integrate(dctrue, lower = lowLim[i], 
#                                                  upper = uppLim[i], 
#                                                  logmod = logmod[i],
#                                                  sigma_tot = sigma_tot[i],
#                                                  eps = eps, Y = Ys[i], 
#                                                  alpha = alphas[i],
#                                                  beta = betas[i])$value } )(icalc)
      zdctruenorm[icalc] <- Vectorize( function(i) {
                    out <- tryCatch({
                                        integrate(dctrue, lower = lowLim[i], 
                                                  upper = uppLim[i], 
                                                  logmod = logmod[i],
                                                  sigma_tot = sigma_tot[i],
                                                  eps = eps, Y = Ys[i], 
                                                  alpha = alphas[i],
                                                  beta = betas[i])$value
                       }, error = function(cond) {
                                       message("Problem encountered in the integration for calculating the likelihood of detections with parameters:\n ",
                                               "lower: ", lowLim[i],
                                               "upper: ", uppLim[i],
                                               "logmod: ", logmod[i],
                                               "sigma_tot: ",sigma_tot[i],
                                               "eps: ", eps, 
                                               "Y: ", Ys[i],
                                               "alpha: ", alphas[i],
                                               "beta: ", betas[i],
                                               "\n Error message was", cond, "\n")
                                       return(-Inf)
                    })
                   return(out)
                    } )(icalc)
      lltruendet_cmod <- Vectorize( function(i) {
                                     ptruendet_cmod(L_C = L_C[i], 
                                                    logmod = logmod[i], 
                                                    sigma_tot = sigma_tot[i], 
                                                    eps = eps, Y = Ys[i], 
                                                    alpha = alphas[i], 
                                                    beta = betas[i],
                                                    zdctruenorm = zdctruenorm[i])
                                        } )(icalc)
# BELOW SHOULD GIVE THE SAME RESULT
if(FALSE) {
      lltruendet_cmod <- Vectorize( function(i) {
                                    integrate(dctrue, lower = 0, upper = L_C[i], 
                                              logmod = logmod[i], 
                                              sigma_tot = sigma_tot[i], eps = eps, 
                                              Y = Ys[i], alpha = alphas[i], 
                                              beta = betas[i])$value / 
                                    zdctruenorm[i]} )(icalc)   
}
      lltruedet_cmod <- 1 - lltruendet_cmod
# BELOW SHOULD GIVE THE SAME RESULT
if(FALSE) {
      lltruedet_cmod <- Vectorize( function(i) {
                                     ptruedet_cmod(L_C = L_C[i],
                                                    logmod = logmod[i],
                                                    sigma_tot = sigma_tot[i],
                                                    eps = eps, Y = Ys[i],
                                                    alpha = alphas[i],
                                                    beta = betas[i],
                                                    zdctruenorm = zdctruenorm[i],
                                                    upper = uppLim[i])
                                        } )(icalc)
}
      # If any prob is negative (due to integration inaccuracy), then ll can be log(-|x|)
      if(any(lltruendet_cmod < 0)) {
        message('Probability lltruendet_cmod contains negative(s):', lltruendet_cmod)
        lltruendet_cmod[which(lltruendet_cmod < 0)] <- 0 
        #browser()
      }
      if(any(lltruedet_cmod < 0)) {
        message('Probability lltruedet_cmod contains negative(s):', lltruedet_cmod)
        lltruedet_cmod[which(lltruedet_cmod < 0)] <- 0 
        #browser()
      }
      # The likelihood of detecting c_det given a true non-detection (false alarm)
      llcdet_truendet <- pcdet_truendet(cdet = ac$obs[icalc], L_C = L_C[icalc])
      # The likelihood of detecting c_det given c_mod (true detection)
      llcdet_cmod <- dctrue(ctrue = ac$obs[icalc], logmod = logmod[icalc], 
                            sigma_tot = sigma_tot[icalc], eps = eps, Y = Ys[icalc],
                            alpha = alphas[icalc], beta = betas[icalc])
      likes[icalc] <- log(llcdet_cmod * lltruedet_cmod + 
                          llcdet_truendet * lltruendet_cmod)
    } # end 'slow' calculations
  } # end calculations
  likes
}


# Likelihood for non-detections -------------------------------------------------
ll_Yee2017log_nondet <- function (ac, alphas = 1/pi, betas = 1, Ys = NULL,
                               ll_noRelease = NULL) {
  # Init
  nsamples <- length(ac$obs)
  if(is.null(Ys)) {
    if(length(alphas) == 1) alphas <- rep(alphas, len = nsamples)
    if(length(betas) == 1) betas <- rep(betas, len = nsamples)
    Ys <- alphas^betas * gamma(betas+0.5) / (sqrt(2*pi) * gamma(betas))
  }
  eps <- 10^-6
  MDC <- ac$MDC
  L_C <- MDC/2
  likes <- numeric(len = nsamples)

  # If precalculated values of ll exist (for ac$mod == 0), then make use of it
  # Else, treat as if all ac$mod != 0, thus calc ll here
  if( !is.null(ll_noRelease)) {
    iprecalc <- which(ac$mod==0)
    icalc <- which(ac$mod!=0)
    likes[iprecalc] <- ll_noRelease[iprecalc]
  } else {
    icalc <- 1:nsamples
  }

  # Calculate ll
  if( length(icalc) > 0 ) {
    ac$mod <- pmax(ac$mod,0.01*ac$MDC) # Some problems for very small ac$mod though not for ac$mod=0 ?1 This seems to be a quick fix
    logmod <- log(ac$mod+eps)

    # below: optimal limits for the integration
    lowLim <- ifelse(ac$mod < MDC, 0, pmax(0,ac$mod-100*ac$sigma_mod) )
    uppLim <- ifelse(ac$mod < MDC, MDC*10, 
                     pmax(MDC*10,ac$mod + pmin(10*ac$mod,100*ac$sigma_mod)))
    zdctruenorm <- rep(1., len = nsamples)
#    zdctruenorm[icalc] <- Vectorize( function(i) {
#                                      integrate(dctrue, lower = lowLim[i], 
#                                                upper = uppLim[i], 
#                                                logmod = logmod[i],
#                                                sigma_tot = ac$sigma_mod[i], 
#                                                eps = eps, Y = Ys[i], 
#                                                alpha = alphas[i],
#                                                beta = betas[i])$value} )(icalc)
    zdctruenorm[icalc] <- Vectorize( function(i) {
               out <- tryCatch({
                                      integrate(dctrue, lower = lowLim[i], 
                                                upper = uppLim[i], 
                                                logmod = logmod[i],
                                                sigma_tot = ac$sigma_mod[i], 
                                                eps = eps, Y = Ys[i], 
                                                alpha = alphas[i],
                                                beta = betas[i])$value
               }, error = function(cond) {
                 message("\nProblem encountered in the integration for calculating the likelihood of non-detections with parameters:",
                         "\nlower: ", lowLim[i],
                         "\nupper: ", uppLim[i],
                         "\nlogmod: ", logmod[i],
                         "\nsigma_mod: ",ac$sigma_mod[i],
                         "\neps: ", eps,
                         "\nY: ", Ys[i],
                         "\nalpha: ", alphas[i],
                         "\nbeta: ", betas[i],
                         "\nMDC: ", MDC[i],
                         "\n Error message was", cond, "\n")
                 return(-Inf)
               })
               return(out)
                     } )(icalc)
    # TO DO: upper bound of 10*MDC[i] is far too high, better 5*MDC[i]?
    llndet_cmod <- Vectorize( function(i) {
                               pndet_cmod(L_C = L_C[i], logmod = logmod[i], 
                                          sigma_mod = ac$sigma_mod[i], eps = eps,
                                          Y = Ys[i], alpha = alphas[i], 
                                          beta = betas[i], 
                                          zdctruenorm = zdctruenorm[i]) } )(icalc)
      
    likes[icalc] <- log(llndet_cmod)
  }
  likes

}

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
