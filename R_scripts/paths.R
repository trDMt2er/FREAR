# -----------------------------------------------------------------------------
###############################################################################
#	(C) 2020 Her Majesty the Queen in Right of Canada, as represented by the
#	Minister of Health
#	
#	This program is free software; you can redistribute it and/or modify it 
#	under the terms of the GNU General Public License as published by the Free
#	Software Foundation; either version 3 of the License, or (at your option)
#	any later version.
#	
#	This program is distributed in the hope that it will be useful, but WITHOUT
#	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
#	FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
#	more details.
#	
#	You should have received a copy of the GNU General Public License along 
#	with this program; if not, see <https://www.gnu.org/licenses>.
###############################################################################
# source('FREAR/R_scripts/paths.R')
# -----------------------------------------------------------------------------

#  srsfilelists <- sprintf("%s/srsfilelist_%s%s.dat", expdir, subexp, if(!is.null(members)) sprintf("_%s",members))

createPaths <- function (datadir, srsfilelists, members = NULL) {
  paths <- lapply(1:length(srsfilelists), function(i) readPaths(srsfilelist = srsfilelists[i], datadir = datadir, member = members[[i]]))
  # check if files exist
  dump <- lapply(paths, checkPaths)
  if(!is.null(members)) names(paths) <- members
  paths
}


# $ ecmwf-ctbto (one srs file for each sample)
#      srs1.1 --> sample1
#      srs2.1 --> sample2
# ...
# $ cmc (four srs files for each sample)
#     srs1.1 srs1.2 srs1.3 srs1.4 --> sample1
#     srs2.1 srs2.2 srs2.3 srs2.4 --> sample2

readPaths <- function(srsfilelist, datadir, member = NULL) {
  # read paths-file
  paths <- read.table(srsfilelist, stringsAsFactors = FALSE)
  nsamples <- nrow(paths)
  paths <- unname(unlist(paths))
  paths <- paste0(datadir, ifelse(is.null(member),'',paste0('/',member)), '/', paths)
  array(paths,dim=c(nsamples,length(paths)/nsamples))
}


checkPaths <- function(paths) {
  # check if files exist
  if(any(!file.exists(paths))) {
    message('The following srs files do not exist:')
    print(paths[!file.exists(paths)])
    stop('There are missing srs files!')
  }
}

createProcessedPaths <- function(processedSRSdir, srsfilelists, members = NULL) {
  if(is.null(processedSRSdir)) {
    NULL
  } else {
    f <- function(srsfilelist, processedSRSdir, member = NULL) {
      fnames <- read.table(srsfilelist, stringsAsFactors = FALSE)
      nsamples <- nrow(fnames)
      fnames <- unname(unlist(fnames))
      if(all(grepl("/output", fnames))) fnames <- gsub("/output","",fnames)
      fnames <- paste0(processedSRSdir, ifelse(is.null(member),'',paste0('/',member)), '/', fnames, ".RDS")
      array(fnames,dim=c(nsamples,length(fnames)/nsamples))
    }
    paths <- lapply(1:length(srsfilelists), function(i) f(srsfilelist = srsfilelists[i], processedSRSdir = processedSRSdir, member = members[[i]]))
    if(!is.null(members)) names(paths) <- members
    paths
  }
}


##############################################################################################################
