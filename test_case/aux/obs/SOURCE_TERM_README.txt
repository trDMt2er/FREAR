Hypothetical forward MLDP simulation
(GEM-GDPS analyses, 0.25deg, 10M particles)

Source:
59.9N 117W (actually a small 10000 ha rectangle - see vertices'
coordinates given below)
02Aug2019
12-15UTC
1e9 Bq Cs137 (resuspended 0-500m above ground by forest fire)

Emission Release amount determined using values from Wotawa et al (2006) Geoph.
Res. Letters. Resuspension rate over N.America was estimated to be
1.2 to 2.3 x10^5 Bq / ha
For a 10000 ha fire,  a total emission of 1e9 Bq is a
reasonable value. The three-hour emission duration might be a bit
short.

For this simulation, 10M Lagrangian particles were used, released initially
within the polygon between the ground and 500m height.

MLDP.2.20190802.1200 INFO

Model                                          = MLDPn
Simulation state                               = 0
Experience number                              = 01
Simulation number                              = 1
Previous simulation                            = -1
Experiment name                                = Cs137_FF_resuspension
Location name                                  = Hwy_35N
Coordinates                                    = {59.894938 -117.137477 59.885098 -116.954118 59.961537 -116.925771 59.970888 -117.120235}
Altitude                                       = 0
Gauss                                          = TRUE
Type of event                                  = TEST/EXERCISE
Request by                                     = Internal
Launch by                                      = 
Launch date                                    = Mon Dec 02 18:22:19 UTC 2019
Accident date                                  = Thu Aug 01 00:00:00 UTC 2019
Simulation date                                = Thu Aug 01 00:00:00 UTC 2019
Initial simulation date                        = Thu Aug 01 00:00:00 UTC 2019
Initial meteorological model data              = 
Simulation duration (h)                        = 501
Backward mode                                  = False
Advection method                               = EULER
Mode                                           = diag
Meteorological model                           = glb
Time interval of meteo files (h)               = 3
Grid name                                      = nhemi2_LL_1440x281_0.25x0.25 (LL 0.2500000 deg, 1440x281)
Grid definition                                = LL 1441 361 0.0000000 -180.0000000 90.0000000 180.0000000 27780.00 0.2500000
Diffusion kernel                               = ORDER0
Order switch (s)                               = 0
Model time step (s)                            = 600
Diffusion time step min value (s)              = 1.0
Diffusion time step over Lagrangian time scale = 1.0
Diffusion factor                               = 1.0
Horiz. wind velocity variance (m²/s²)          = 1.00
Lagrangian time scale (s)                      = 10800
Type of source                                 = ACCIDENT
Lower layer thickness (m)                      = 500
Output time step (min)                         = 360
Output file delta                              = 24
Concentration vertical Levels (m)              =  1 500
Aviations vertical Levels (ft)                 =  0 20000 35000 60000 85000 110000 135000 
Variables to save                              = CV
Bottom reflection level (hyb|eta|sig)          = 0.9999
Initial seed                                   = VARIABLE
Release scenario                               = {Cs137_FF_resusp
Decay chain                                    = 0
Aerosol type                                   = URBAN
Wet scavenging mode                            = DEFAULT
Dry deposition mode                            = DEFAULT
