The main program files are in R_scripts. An inference is normally configured in another directory (e.g. test_case). This path contains files where user input is normally performed (i.e. configuring main.R for scenario). For example:

To test your installation:

1. Modify paths in test_case/expdir/main.R
2. source main.R

The program should run with both text and graphic output.

Optionally install meteogrid in R for marginal posterior map output

3. install.packages(c("remotes", "maps")); remotes::install_github("adeckmyn/meteogrid")
4. modify R_scripts/settings.R and set "lusemeteogrid"=TRUE
5. source main.R

Technical information can be found in the following publication:
De Meutter, P., & Hoffman, I. (2020). Bayesian source reconstruction of an anomalous Selenium-75 release at a nuclear research institute. Journal of Environmental Radioactivity, 218, 106225.
A pdf copy is also provided in the /docs folder (file TechnicalInformation_FREARtool.pdf)

